import * as http from 'http'

import App from './app'

const port = normalizePort(process.env.PORT || 5000)
App.set('port', port)

const server = http.createServer(App)
server.listen(port, null, () => console.log(`🚀 Server ready at ${port}`))
server.on('error', onError)

function normalizePort(val: number|string): number|string|boolean {
  const result: number = (typeof val === 'string') ? parseInt(val, 10) : val

  if (isNaN(result)) { return val }
  if (result >= 0) { return result }

  return false
}

function onError(error: NodeJS.ErrnoException): void {
  if (error.syscall !== 'listen') { throw error }

  const bind = (typeof port === 'string') ? 'Pipe ' + port : 'Port ' + port

  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`)
      process.exit(1)
      break
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`)
      process.exit(1)
      break
    default:
      throw error
  }
}
