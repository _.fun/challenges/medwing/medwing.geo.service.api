
import { NextFunction, Request, Response, Router} from 'express'

import { IController } from '@/services/api'

export class GeoController implements IController {
  public path: string = '/geo'
  public router: Router

  constructor() {
    this.router = Router()
    this.router.get('/', this.get)
  }

  public get(req: Request, res: Response, next: NextFunction) {
    res.send(['data', 'data'])
  }
}
