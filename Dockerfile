FROM node:12.9.0-alpine as builder

COPY package*.json ./
COPY tsconfig.json ./
COPY src src
RUN npm run build:first

FROM node:12.9.0-alpine

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY package*.json ./
COPY --from=builder build build
RUN npm ci --silent

CMD ["npm", "run", "start"]
